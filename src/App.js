import React from "react";
import {Route, Switch} from "react-router-dom";
import TopNavigation from "./components/top-navigation/top-navigation.component";
import ToggleNavigation from "./components/toggle-navigation/toggle-navigation.component";

import Home from "./pages/home/home.page";
import Product from "./pages/product/index/product.page";
import ProductDetail from "./pages/product/detail/product-detail.page";
import SignInSignUp from './pages/sign-in-sign-up/sign-in-sign-up.page';

import {auth, createUserProfileDocument} from "./firebase/firebase.utils";

const categories = {
    store: [
        {
            id: 1,
            name: 'MacBook Air',
            product: [
                {
                    id: 1,
                    name: 'MacBook Air 2018 128GB (Space Gray)',
                    url: 'macbook-air-2018-128gb-space-gray',
                    imgPath: '/images/products/MacBookAir-2018-SpaceGray.jpg',
                    oldPrice: null,
                    price: 18000000
                },
                {
                    id: 2,
                    name: 'MacBook Air 2018 128GB (Silver)',
                    url: 'macbook-air-2018-128gb-silver',
                    imgPath: '/images/products/MacBookAir-2018-SpaceGray.jpg',
                    oldPrice: null,
                    price: 18000000
                },
                {
                    id: 3,
                    name: 'MacBook Air 2018 128GB (Gold)',
                    url: 'macbook-air-2018-128gb-gold',
                    imgPath: '/images/products/MacBookAir-2018-SpaceGray.jpg',
                    oldPrice: null,
                    price: 18000000
                },
                {
                    id: 4,
                    name: 'MacBook Air 2019 128GB (Space Gray)',
                    url: 'macbook-air-2019-128gb-space-gray',
                    imgPath: '/images/products/MacBookAir-2018-SpaceGray.jpg',
                    oldPrice: null,
                    price: 18000000
                },
                {
                    id: 5,
                    name: 'MacBook Air 2019 128GB (Silver)',
                    url: 'macbook-air-2019-128gb-silver',
                    imgPath: '/images/products/MacBookAir-2018-SpaceGray.jpg',
                    oldPrice: null,
                    price: 18000000
                },
                {
                    id: 6,
                    name: 'MacBook Air 2019 128GB (Gold)',
                    url: 'macbook-air-2019-128gb-gold',
                    imgPath: '/images/products/MacBookAir-2018-SpaceGray.jpg',
                    oldPrice: null,
                    price: 18000000
                },
            ],
        },
        {
            id: 2,
            name: 'MacBook Pro',
            product: [
                {
                    id: 1,
                    name: 'MacBook Pro 2019 13-inch TouchBar (Space Gray)',
                    url: 'macbook-air-2018-128gb-space-gray',
                    imgPath: '/images/products/MacBookAir-2018-SpaceGray.jpg',
                    oldPrice: null,
                    price: 18000000
                },
            ]
        },
    ],
    accessories: [
        {
            id: 1,
            name: 'Apple Accessories'
        },
        {
            id: 2,
            name: 'Microsoft Application'
        },
        {
            id: 3,
            name: 'Charger'
        },
    ],
};



class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggleNav: true,
            currentUser: null
        };
    }

    toggleButtonOnClick = () => {
        this.state.toggleNav
            ? this.setState({toggleNav: false})
            : this.setState({toggleNav: true});
    };


    componentDidMount() {
        auth.onAuthStateChanged(async userAuth => {
            if (userAuth){
                const userRef = await createUserProfileDocument(userAuth);
                userRef.onSnapshot(snapshot => {
                    this.setState({
                        currentUser: {
                            id: snapshot.id,
                            ...snapshot.data
                        }
                    });
                });
            }
            this.setState({currentUser: userAuth});
        });
    }

    render() {
        return (
            <section className="app">
                <TopNavigation
                    toggleButtonOnClick={this.toggleButtonOnClick}
                    categories={categories}
                    currentUser={this.state.currentUser}
                />
                <ToggleNavigation
                    toggleNav={this.state.toggleNav}
                    toggleButtonOnClick={this.toggleButtonOnClick}
                    categories={categories}
                    currentUser={this.state.currentUser}
                />
                <div>
                    <Switch>
                        <Route exact path={"/"}>
                            <Home productList={categories.store} />
                        </Route>
                        <Route exact path={"/product"} component={Product}/>
                        <Route exact path={'/product-best-seller'} component={Product}/>
                        <Route exact path={'/sign-in-or-sign-up'} component={SignInSignUp}/>
                        <Route path={"/product/:productUrl"} component={ProductDetail}/>
                    </Switch>
                </div>
            </section>
        );
    }
}

export default App;
