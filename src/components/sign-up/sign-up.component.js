import React from 'react';
import './sign-up.style.scss';
import FormInput from "../form-input/form-input.component";
import CustomButton from "../custom-button/custom-button.component";

class SignUp extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
        }
    }

    handleChange = (e) =>{
        const {value, name} = e.target;
        this.setState({[name]: value});
    };

    handleSubmit = (e) =>{
        e.preventDefault();
        this.setState({name: '', email: '', password: '', confirmPassword: ''});
    };

    render() {
        return(
            <section className={'signUp'}>
                <h2>I don&apos;t have an account</h2>
                <span>Sign up with your email and password.</span>
                <form onSubmit={this.handleSubmit}>
                    <FormInput
                        name={'name'}
                        type={'text'}
                        value={this.state.name}
                        handleChange={this.handleChange}
                        label={'Name'}
                        required />
                    <FormInput
                        name={'email'}
                        type={'email'}
                        value={this.state.email}
                        handleChange={this.handleChange}
                        label={'Email'}
                        required />
                    <FormInput
                        name={'password'}
                        type={'password'}
                        value={this.state.password}
                        handleChange={this.handleChange}
                        label={'Password'}
                        required />

                    <FormInput
                        name={'confirmPassword'}
                        type={'password'}
                        value={this.state.confirmPassword}
                        handleChange={this.handleChange}
                        label={'Confirm Password'}
                        required />
                    <CustomButton type={'submit'} style={{width: '50%'}}>Sign Up</CustomButton>
                </form>
            </section>
        )
    }
}

export default SignUp;