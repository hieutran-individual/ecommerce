import React from 'react';
import PropTypes from 'prop-types';
import LinkDropDown from "../link-dropdown/link-dropdown.component";
import './toggle-navigation.style.scss';

export default function ToggleNavigation(props){
    const navStyle = {
        width: (props.toggleNav) ? '0' : '300px',
        padding: (props.toggleNav) ? '0' : '1em',
    };
    return(
        <nav className={'toggleNav shadow'} style={navStyle}>
            <button className={'toggleNav__close'} onClick={props.toggleButtonOnClick}>x</button>
            {
                Object.keys(props.categories).map(
                    (key) =>
                        <LinkDropDown
                            key={key}
                            toggleNav={props.toggleNav}
                            inToggleNavigation={true}
                            overrideLinkStyle={{
                                color: '#000',
                                textAlign: 'right'
                            }}
                            overrideContainerStyle={{
                                alignItems: 'flex-end',
                                marginTop: '1em',
                            }}
                            primary={{id: 1, name: key}}
                            links={props.categories[key]} />
                )
            }
        </nav>
    );
}

ToggleNavigation.propTypes = {
    categories: PropTypes.array,
    productTypes: PropTypes.array,
    toggleNav: PropTypes.bool,
    toggleButtonOnClick: PropTypes.func
};