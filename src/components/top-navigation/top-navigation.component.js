import React from "react";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faSearch,
    faBars,
    faShoppingCart
} from "@fortawesome/free-solid-svg-icons";
import LinkDropDown from "../link-dropdown/link-dropdown.component";
import {Link} from "react-router-dom";
import "./top-navigation.style.scss";

import {auth} from "../../firebase/firebase.utils";

export default function TopNavigation(props) {

    const handleSignOut = (e) => {
        e.preventDefault();
        auth.signOut();
    };

    return (
        <div className={"containerTopNav"}>
            <nav className={"topNav"}>
                <Link className={"topNav__logo"} to={"/"}>
                    <img src={"/images/logo.svg"} alt={"logo"}/>
                </Link>

                <nav className={"topNav__links"}>
                    {
                        Object.keys(props.categories).map(
                            (key) =>
                                <LinkDropDown
                                    key={key}
                                    overrideLinkStyle={{
                                        color: "#000"
                                    }}
                                    overrideContainerStyle={{}}
                                    primary={{id: 1, name: key}}
                                    links={props.categories[key]}
                                />
                        )
                    }
                    {
                        props.currentUser ?
                            <a className={'topNav__links__signOut'} onClick={handleSignOut}>Sign Out</a> :
                            <Link className={'topNav__links__signOut'} to={'/sign-in-or-sign-up'}>Sign In</Link>
                    }
                </nav>

                <div className={"topNav__function"}>
                    <button className={"topNav__function__search"}>
                        <FontAwesomeIcon
                            icon={faSearch}
                            className={"topNav__function__icon"}
                        />
                    </button>
                    <button className={"topNav__function__cart"}>
                        <FontAwesomeIcon
                            icon={faShoppingCart}
                            className={"topNav__function__icon"}
                        />
                    </button>
                    <button
                        onClick={props.toggleButtonOnClick}
                        className={"topNav__function__toggleNav"}
                    >
                        <FontAwesomeIcon
                            icon={faBars}
                            className={"topNav__function__icon"}
                        />
                    </button>
                </div>
            </nav>
        </div>
    );
}

TopNavigation.propTypes = {
    categories: PropTypes.object,
    toggleButtonOnClick: PropTypes.func,
    currentUser: PropTypes.object
};
