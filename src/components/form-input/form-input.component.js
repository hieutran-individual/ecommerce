import React from 'react';
import PropTypes from 'prop-types';
import './form-input.style.scss';
export default function FormInput({handleChange, label, ...otherProps }){
    return(
        <div className={'formGroup'}>
            <input
                className={'formGroup__input'}
                onChange={handleChange}
                {...otherProps}
            />
            {
                label ?
                    <label className={`${otherProps.value.length ? 'shrink' : ''} formGroup__label`}>
                        {label}
                    </label>
                    : null
            }
        </div>
    )
}

FormInput.propTypes = {
    handleChange: PropTypes.func,
    label: PropTypes.string,
    otherProps: PropTypes.array
};