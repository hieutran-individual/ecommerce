import React, {useState} from "react";
import Carousel from 'react-bootstrap/Carousel';
import 'bootstrap/dist/css/bootstrap.min.css';
import './landing.style.scss';

export default function Landing(){
    const [index, setIndex] = useState(0);
    const [direction, setDirection] = useState(null);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
        setDirection(e.direction);
    };

    return(
        <div className={'landing'}>
            <Carousel activeIndex={index} direction={direction} onSelect={handleSelect}>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/images/landing/landing_1.jpg"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3 className={'text-shadow'}>First slide label</h3>
                        <p className={'text-shadow'}>Nulla vitae elit libero.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/images/landing/landing_2.jpg"
                        alt="Second slide"
                    />

                    <Carousel.Caption>
                        <h3 className={'text-shadow'}>Second slide label</h3>
                        <p className={'text-shadow'}>Lorem ipsum dolor sit amet.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/images/landing/landing_3.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3 className={'text-shadow'}>Third slide label</h3>
                        <p className={'text-shadow'}>
                            Praesent commodo cursus magna.
                        </p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    )
}