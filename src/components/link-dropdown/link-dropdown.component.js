import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import './link-dropdown.style.scss';

export default function LinkDropDown(props){
    const primary = props.primary;
    const links = props.links;
    const isInToggleNavigation = props.inToggleNavigation;
    let navStyle;
    const [toggleNav, setToggleNav] = useState(true);

    if (isInToggleNavigation) {
        navStyle = {...props.overrideContainerStyle};
    } else {
        navStyle = {
            position: 'absolute',
            left: '1em',
            top: '2em',
            backgroundColor: '#fff',
            boxShadow: '2px 2px 76px -32px rgba(0,0,0,0.59)',
            boxSizing: 'border-box',
            borderRadius: '5px',
        };
    }

    const handlePrimaryLinkOnClick = (e) => {
        e.preventDefault();
        (toggleNav) ? setToggleNav(false) : setToggleNav(true);
    };

    const handlePrimaryLinkOnBlur = () => {
        setToggleNav(true);
    };

    return(
        <section className="linkDropDown" onMouseEnter={handlePrimaryLinkOnClick} onMouseLeave={handlePrimaryLinkOnBlur} style={props.overrideContainerStyle}>
            <Link
                to={'#'}
                className="linkDropDown__primary"
                style={props.overrideLinkStyle}>{primary.name}</Link>
            <nav
                className="linkDropDown__containers"
                style={ (toggleNav && !props.toggleNav) ? navStyle : {...navStyle, maxHeight: '500px'} }>
            {links.map(link => (
                <Link
                    to={'#'}
                    key={link.id}
                    style={props.overrideLinkStyle}>{link.name}</Link>
            ))}
            </nav>
        </section>
    );
}

LinkDropDown.propTypes = {
    primary: PropTypes.object,
    links: PropTypes.array,
    overrideLinkStyle: PropTypes.object,
    overrideContainerStyle: PropTypes.object,
    inToggleNavigation: PropTypes.bool,
    toggleNav: PropTypes.bool
};