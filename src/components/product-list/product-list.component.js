import React from "react";
import PropTypes from 'prop-types';
import ProductBlock from "../product-block/product-block.component";

export default function ProductList(props){
    return(
        <section className={'productList'}>
            {
                props.productList.map(
                    (product, index) =>
                        <ProductBlock
                            key={index}
                            url={product.url}
                            imgPath={product.imgPath}
                            name={product.name}
                            oldPrice={product.oldPrice}
                            price={product.price}  />
                )
            }
        </section>
    );
}

ProductList.propTypes = {
    productList: PropTypes.array
}