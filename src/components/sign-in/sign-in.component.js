import React from 'react';
import {SignInWithGoogle} from '../../firebase/firebase.utils.js';
import './sign-in.style.scss';
import FormInput from "../form-input/form-input.component";
import CustomButton from "../custom-button/custom-button.component";

class SignIn extends React.Component{
    constructor(props) {
        super(props);
        this.state= {
            email: '',
            password: ''
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({email: '', password: ''});
    };

    handleChange = e =>{
        const {value, name} = e.target;
        this.setState({[name]: value});
    };

    render() {
        return(
            <div className={'signIn'}>
                <h2>I already have an account</h2>
                <span>Sign in with your email and password.</span>
                <form onSubmit={this.handleSubmit}>
                    <FormInput
                        name={'email'}
                        type={'email'}
                        value={this.state.email}
                        handleChange={this.handleChange}
                        label={'Email'}
                        required />
                    <FormInput
                        name={'password'}
                        type={'password'}
                        value={this.state.password}
                        handleChange={this.handleChange}
                        label={'Password'}
                        required />
                    <CustomButton type={'submit'} style={{width: '50%'}}>Sign In</CustomButton>
                    <CustomButton
                        className={'customButton googleButton'}
                        style={{width: '50%', backgroundColor: '#4f83fb', borderColor: '#4f83fb', '&:hover': {backgroundColor: '#fff'}}}
                        onClick={SignInWithGoogle}
                    >Sign in with Google</CustomButton>
                </form>
            </div>
        );
    }
}
export default SignIn;
