import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './product-block.style.scss';

export default function ProductBlock(props){
    return(
        <Link to={props.url} className={'productBlock'}>
            <figure>
                <img src={props.imgPath} alt={props.name} />
            </figure>
            <h4>{props.name}</h4>
            <p><del>{props.oldPrice}</del> {props.price}</p>
        </Link>
    )
}

ProductBlock.propTypes = {
    url: PropTypes.string,
    name: PropTypes.string,
    oldPrice: PropTypes.number,
    price: PropTypes.number,
    imgPath: PropTypes.string
};