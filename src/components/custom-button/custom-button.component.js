import React from 'react';
import PropTypes from 'prop-types';
import './custom-button.style.scss';

const CustomButton = ({children, ...otherProps}) =>{
    return(
        <button className={'customButton'} {...otherProps} >
            {children}
        </button>
    )
};

CustomButton.propTypes = {
    children: PropTypes.any,
};

export default CustomButton;