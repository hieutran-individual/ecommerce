import React from "react";
import PropTypes from 'prop-types';
import Landing from "../../components/landing/landing.component";
import ProductList from "../../components/product-list/product-list.component";

export default function Home(props){
    return(
        <div className={'home'}>
            <Landing/>
            <ProductList productList={props.productList} />
        </div>
    )
}

Home.propTypes = {
    productList: PropTypes.array
};