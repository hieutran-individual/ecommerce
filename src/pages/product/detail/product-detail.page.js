import React from 'react';

import './product-detail.style.scss';


export default function ProductDetail(props){
    console.log(props);
    return(
        <div className={'productDetail'}>
            <p>Product Detail Page {props.match.params.productUrl}</p>
        </div>
    );
}