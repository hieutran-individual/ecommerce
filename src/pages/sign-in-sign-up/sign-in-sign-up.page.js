import React from 'react';
import SignIn from "../../components/sign-in/sign-in.component";
import './sign-in-sign-up.style.scss';
import SignUp from "../../components/sign-up/sign-up.component";

export default function SignInSignUp(){
    return (
        <section className={'signInSignUp shadow'}>
            <SignIn />
            <SignUp />
        </section>
    )
}