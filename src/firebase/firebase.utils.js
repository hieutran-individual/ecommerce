import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyCLKwwOY8O3R5bGLeovMXBBFazBUctgxe0",
    authDomain: "e-comerce-29540.firebaseapp.com",
    databaseURL: "https://e-comerce-29540.firebaseio.com",
    projectId: "e-comerce-29540",
    storageBucket: "e-comerce-29540.appspot.com",
    messagingSenderId: "628004919857",
    appId: "1:628004919857:web:a65427bc288b83ec1b9310",
    measurementId: "G-WVH7NL0ZFZ"
};
firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;
    const userRef = firestore.doc(`user/${userAuth.uid}`);
    const snapshot = await userRef.get();

    if (!snapshot.exists){
        const {displayName, email} = userAuth;
        const createdAt = new Date();
        try{
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            });
        } catch(error){
            console.log('creating user has failed.', error.message);
        }
    }
    return userRef;
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});
export const SignInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;